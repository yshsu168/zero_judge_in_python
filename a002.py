# https://zerojudge.tw/ShowProblem?problemid=a002
# 簡易加法
# INPUT => OUTPUT
# 5 10 => 15
# 1 2 => 3
a = input()
k = a.split(" ")
print(int(k[0])+int(k[1]))
