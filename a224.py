# a224: 明明愛明明
# 計算字元出現的次數，最多只能有一個字元出現奇數次，其它只能出現偶數次。
while True:
    try:
        q = input().lower()
        matched = []
        t_odd = 0
        for i in range(len(q)):
            if q[i].isalpha() and not q[i] in matched:
                matched.append(q[i])
                if q.count(q[i]) % 2:
                    t_odd += 1
                if t_odd > 1:
                    break
        if t_odd > 1:
            print("no...")
        else:
            print("yes !")

    except EOFError:
        break
