# a225: 明明愛排列
while True:
    try:
        num = int(input())
        q =  list(map(int, input().split(" ")))
        res = []
        for x in range(10):
            tmp_q = []
            for i in q:
                if i % 10 == x:
                    tmp_q.append(i)
            tmp_q.sort(reverse=True)
            res = res + tmp_q
        for i in range(len(res)-1):
            print(res[i], end=" ")
        print(res[-1])

    except EOFError:
        break
