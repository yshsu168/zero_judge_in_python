# https://zerojudge.tw/ShowProblem?problemid=a034
# 二進位制轉換
# INPUT => OUTPUT
# 3 => 11
# 6 => 110
while True:
  try:
     a=int(input())
     b=bin(a)
     print(b[2:])
  except:
     break
