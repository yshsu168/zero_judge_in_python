# a148: You Cannot Pass?!
while True:
  try:
    scores = list(map(int, input().split(" ")))
    if scores[0] != len(scores) -1:
        print("something wrong")
    else:
        avg = sum(scores[1:]) / scores[0]
        if avg > 59:
            print("no")
        else:
            print("yes")
  except EOFError:
    break

