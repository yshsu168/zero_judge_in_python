# a104: 排序
while True:
  try:
    num = int(input()) # useless
    # if you don't translate into int, there will be problem with different string length
    data = list(map(int, input().split(" ")))
    data.sort()
    out = ""
    for i in range(len(data)-1):
      out = out + str(data[i]) + " "
    out = out + str(data[-1])
    print(out)
  except EOFError:
    break

