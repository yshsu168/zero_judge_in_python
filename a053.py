# Sagit's 計分程式
x = int(input())
p = 0
if x >= 0 and x < 10:
  p = 6 * x
elif x >= 10 and x < 20:
  p = 6 * 10 + ( x - 10) * 2
elif x >= 20 and x < 40:
  p = 6 * 10 + 10 * 2 + (x -20) * 1
else:
  p = 100
print(p)
