# https://zerojudge.tw/ShowProblem?problemid=a038
# 數字翻轉
# INPUT => OUTPUT
# 12345 => 54321
# 5050 => 505
num=input()
out=""
for i in range(len(num)-1, -1, -1):
  out=out+num[i]
print(int(out))
