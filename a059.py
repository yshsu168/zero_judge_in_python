# a059: 完全平方和
import math
num = int(input())
res=""
for n in range(1,num+1):
    start = int(input())
    end = int(input())
    nsum = 0
    for i in range(start, end + 1):
        x = math.sqrt(i)
        if int(x) == x:
            nsum += i
    res += "Case " + str(n) + ": " + str(nsum) + "\n"
print(res)
