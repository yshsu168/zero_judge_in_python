# https://zerojudge.tw/ShowProblem?problemid=a003
# 兩光法師占卜術
# INPUT => OUTPUT
# 1 1 => 普通
# 1 2 => 吉

ans=["普通", "吉", "大吉"]
a=input()
k=a.split(" ")
s = (int(k[0])*2+int(k[1]))%3
print(ans[s])
