# a647. 投資專家
num = int(input())
out=[]
r = 0.00000000001
for i in range(num):
    item = list(map(int, input().split(" ")))
    x = ((item[1]-item[0])/item[0])
    if x > 0:
        x = x + r
    if x < 0:
        x = x - r
    a = format(x, '.2%')
    if x>=0.1 or x<= -0.07:
        out.append(a + " dispose")
    else:
        out.append(a + " keep")
print("\n".join(out))
