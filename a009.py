# https://zerojudge.tw/ShowProblem?problemid=a009
# 解碼器
# INPUT => OUTPUT
# 1JKJ'pz'{ol'{yhklthyr'vm'{ol'Jvu{yvs'Kh{h'Jvywvyh{pvu5 => *CDC is the trademark of the Control Data Corporation
# 1PIT'pz'h'{yhklthyr'vm'{ol'Pu{lyuh{pvuhs'I|zpulzz'Thjopul'Jvywvyh{pvu5 => *IBM is a trademark of the International Business Machine Corporation.:2
k=7
a=input()
ans=""
for i in range(len(a)):
  ans+= chr(ord(a[i])-7)
print(ans)
