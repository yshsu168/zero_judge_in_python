# https://zerojudge.tw/ShowProblem?problemid=a004
# 文文的求婚
# INPUT => OUTPUT
# 1977 => 平年
# 1980 => 閏年
while True:
  try:
     year=int(input())
     if not year%4 and (year %100 or not year%100):
         print("閏年")
     else:
         print("平年")
  except:
     break

