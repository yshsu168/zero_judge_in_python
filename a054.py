# https://zerojudge.tw/ShowProblem?problemid=a020
# 電話客服中心
# INPUT => OUTPUT
# 130245675 => FS
# 123456789 => AMW
city={ 'A':10, 'J':18, 'S':26, 'B':11, 'K':19, 'T':27 ,\
       'C':12, 'L':20, 'U':28, 'D':13, 'M':21, 'V':29, \
       'E':14, 'N':22, 'W':32, 'F':15, 'O':35, 'X':30, \
       'G':16, 'P':23, 'Y':31, 'H':17, 'Q':24, 'Z':33, \
       'I':34, 'R':25}
id = input()
sum = 0
for i in range(1, 9):
  sum = sum + int(id[-i-1]) * i
sum = sum + int(id[-1])

for i in range(ord('A'), ord('Z')+1):
  k = city[chr(i)]
  final = sum + (k%10)*9 + k//10
  if not final % 10:
    print(chr(i), end="")
