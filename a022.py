# https://zerojudge.tw/ShowProblem?problemid=a022
# 迴文
# INPUT => OUTPUT
# abba => yes
# abcd => no.
a = input()
l = len(a)
mid = l//2 -1
isT=True
for i in range(mid):
    if a[i] != a[-i-1]:
        isT= False
        break
if isT:
    print("yes")
else:
    print("no")
