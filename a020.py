# https://zerojudge.tw/ShowProblem?problemid=a020
# 身份證檢驗
# INPUT => OUTPUT
# T112663836 => real
# S154287863 => fake
city={ 'A':10, 'J':18, 'S':26, 'B':11, 'K':19, 'T':27 ,\
       'C':12, 'L':20, 'U':28, 'D':13, 'M':21, 'V':29, \
       'E':14, 'N':22, 'W':32, 'F':15, 'O':35, 'X':30, \
       'G':16, 'P':23, 'Y':31, 'H':17, 'Q':24, 'Z':33, \
       'I':34, 'R':25}
id = input()
A = (city[id[0]]%10)*9+city[id[0]]//10
sum=0
for i in range(1, 9):
  sum = sum+ int(id[-i-1])*i
final=A+sum+int(id[-1])
if final % 10:
  print("fake")
else:
  print("real")
