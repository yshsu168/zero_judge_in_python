# a121: 質數又來囉 TLE 9s. :(
def isPrime(x):
    if x < 3:
        return(True)
    testPrime=True
    if x%2 == 0:
        testPrime=False
    else:
        for i in range(2, x//2):
            if x%i == 0:
                testPrime=False
    return(testPrime)

while True:
    try:
        a = input().split(" ")
        start = int(a[0])
        end = int(a[1])
        numPrime = 0
        for i in range(start, end+1):
            if isPrime(i):
                numPrime+=1
        print(numPrime)
    except:
        break
