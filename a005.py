# https://zerojudge.tw/ShowProblem?problemid=a005
# Eva 的回家作業
# INPUT => OUTPUT
# 1 2 3 4 => 1 2 3 4 5
# 1 2 4 8 => 1 2 4 8 16

for i in range(int(input())):
    bi=[]
    b= input().split(" ")
    for j in b:
        bi.append(int(j))

    X = bi
    A = bi[1]//bi[0]
    D = bi[1]-bi[0]
    isA=True
    if not D == 0:
        isP=True
    else:
        isP=False
    for j in range(1,len(bi)-1):
        if not A == bi[j+1]//bi[j]:
            isP=False
        if D!=0 and not D == bi[j+1]-bi[j]:
            isA=False
    if isP:
        bi.append(bi[-1]*A)
        ans=""
        for e in range(0,len(bi)):
            if e != len(bi)-1:
                ans = ans + str(bi[e]) + " "
            else:
                ans += str(bi[e])

        print(ans)
    if isA:
        bi.append(bi[-1]+D)
        ans=""
        for e in range(0,len(bi)):
            if e != len(bi)-1:
                ans = ans + str(bi[e]) + " "
            else:
                ans += str(bi[e])

        print(ans)

