# https://zerojudge.tw/ShowProblem?problemid=a040
# 阿姆斯壯數
# INPUT => OUTPUT
# 100 999 => 153 370 371 407
# 10 99 => none
a = input().split(" ")
start=int(a[0])
end=int(a[1])
found=False
for i in range(start, end+1, 1):
    i_str=str(i)
    ll=len(i_str)
    l=ll-1
    res=0
    while l >=0:
        res=res+int(i_str[l])**ll
        l=l-1
    if res == i:
        print(res, end=" ")
        found=True
if not found:
    print("none")
