# https://zerojudge.tw/ShowProblem?problemid=a024
# 給定兩個整數，請求出它們的最大公因數
# INPUT => OUTPUT
# 12 15 => 3
# 1 100 => 1
def GCD (a, b):
      if b == 0: 
         return (a)
      else:
         return(GCD(b, a%b))

a = input().split(" ")
for i in range(len(a)):
    a[i] = int(a[i])

print(GCD(a[0], a[1]))
